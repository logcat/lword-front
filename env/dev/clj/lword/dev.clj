(ns lword.dev
  (:require
    [com.stuartsierra.component :as component]
    [figwheel-sidecar.auto-builder]
    [figwheel-sidecar.system :as sys]
    [figwheel-sidecar.repl-api]
    [clojure.java.shell :refer [sh]]))

(def system
  (component/system-map
    :figwheel-system (sys/figwheel-system (sys/fetch-config))))

(defn start-figwheel []
  (alter-var-root #'system component/start))

(defn start-sass []
  (future
    (println "Starting sass.")
    (sh "lein" "auto" "sassc" "once")))

(start-sass)
(start-figwheel)

(defn browser-repl []
  (sys/cljs-repl (:figwheel-system system)))

