## Build production
```
lein with-profile -dev,+prod cljsbuild once
```

## Deploy
```
firebase deploy
```

## Usage
```
http://localhost:3449/#/en/uk/snow
```
```
https://lword.firebaseapp.com/#/en/uk/snow
```