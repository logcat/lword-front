(defproject lword-front "0.1.0-SNAPSHOT"
  :description "LWord frontend"

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.170"]
                 [org.omcljs/om "0.9.0"]
                 [prismatic/om-tools "0.3.12"]
                 [cljsjs/react-dom "0.14.3-1"]
                 [cljsjs/react-dom-server "0.14.3-0"]
                 [sablono "0.5.3"]
                 [secretary "1.2.3"]]

  :plugins [[lein-cljsbuild "1.1.1"]
            [lein-sassc "0.10.4"]
            [lein-auto "0.1.1"]]

  :clean-targets ^{:protect false} ["resources/public/js"]

  :sassc [{:src       "src/scss/style.scss"
           :output-to "resources/public/css/style.css"}]

  :cljsbuild {:builds {:app {:source-paths ["src/cljs"]
                             :compiler     {:main       lword.main
                                            :output-to  "resources/public/js/main.js"
                                            :output-dir "resources/public/js/"
                                            :source-map "resources/public/js/out.js.map"}}}}

  :auto {"sassc" {:file-pattern #"\.(scss)$"}}

  :profiles {:dev  {:source-paths ["env/dev/clj"]
                    :dependencies [[figwheel-sidecar "0.5.0"]
                                   [org.clojure/tools.nrepl "0.2.10"]
                                   [com.cemerick/piggieback "0.2.1"]]
                    :repl-options {:init-ns          lword.dev
                                   :nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                    :cljsbuild    {:builds {:app {:compiler {:optimizations :none
                                                             :pretty-print  true}}}}}

             :prod {:cljsbuild {:builds {:app {:compiler {:optimizations :advanced
                                                          :pretty-print  false}}}}}})
