(ns ^:figwheel-always lword.main
  (:require [om.core :as om]
            [secretary.core :as secretary :refer-macros [defroute]]
            [lword.components :refer [ui]]
            [goog.events :as events]
            [goog.history.EventType :as EventType]
            [lword.models])
  (:import goog.History))

(defonce app-state
         (atom
           {:translation-request {}
            :description         {}}))


(secretary/set-config! :prefix "#")

(defroute "/:from/:to/:word" {:as translation-request}
          (do
            (swap! app-state assoc :translation-request (lword.models/map->TranslationRequest translation-request) :description {})
            (om/root ui app-state
                     {:target (.getElementById js/document "app")})))

(let [h (History.)]
  (events/listen h EventType/NAVIGATE #(secretary/dispatch! (.-token %)))
  (doto h (.setEnabled true)))