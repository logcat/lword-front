(ns ^:figwheel-always lword.components
  (:require [om.core :as om]
            [om-tools.core :refer-macros [defcomponent]]
            [sablono.core :as html :refer-macros [html]]
            [lword.api :refer [translate]]))

(defcomponent word [word]
  (render [_]
    (html [:div word])))

(defcomponent description [{:keys [translation image audio]}]
  (render [_]
    (html [:div
           [:div translation]
           [:img {:src image}]
           [:div
            (if (some? audio)
              [:audio {:autoPlay "true"
                       :controls "controls"
                       :preload  "auto"}
               [:source {:src  audio
                         :type "audio/mpeg"}]])]])))

(defcomponent ui [app-state]
  (will-mount [_] (translate app-state (:translation-request @app-state)))
  (render [_]
    (html [:div
           (om/build word (:word (:translation-request app-state)))
           (let [description-data (:description app-state)]
             (when (some? description-data)
               (om/build description description-data)))])))