(ns ^:figwheel-always lword.handlers
  (:require [om.core :refer [transact!]]
            [lword.models]))

(defn translation-handler [app-state response]
  (transact! app-state :description (fn [_] (lword.models/map->Description response))))
