(ns ^:figwheel-always lword.api
  (:require [lword.handlers :refer [translation-handler]]
            [goog.net.XhrIo :as xhr]
            [goog.json.Serializer])
  (:import [goog Uri]
           [goog.net Jsonp]))

(defn xhrio->response
  [xhrio]
  (-> xhrio .-target .getResponseJson (js->clj :keywordize-keys true)))

(defn clj->json
  [data]
  (.stringify js/JSON (clj->js data)))

(defn POST [url handler data]
  (let [handler #(handler (xhrio->response %))
        json (clj->json data)
        headers #js {"Content-Type" "application/json"}]
    (xhr/send url handler "POST" json headers)))

(defn translate [app-state translation-request]
  (POST "https://lword.herokuapp.com/word" (partial translation-handler app-state) translation-request))